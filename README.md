### Git Branching 

Method of version control that allows you to make parrallel timelines

Value is: Work on your own timeline without destroying the master timeline

Branching is the branching of the main timeline `master`

### Main commands

```bash
# Create a new brach from master 
# Shoul d be in master and updated 
$ git pull origin master

## Create your new branch 
### Use good naming convention
$ git checkout -b <branch-name>

### Name convention differs from team to team 
## One good things is to start with dev-feature 
$ git checkout -b dev-front-page

# Make changes, git add, git commit, push branch 
$ git add .
$ git commit 
$ git push origin dev-front-page

```

### New rules not to get fired 
- Dont push master 
- If you are a devops engineer - DEFINETLY DONT PUSH TO MASTER
- Find out how to protect it and have jenkins be the only allowed to edit master + senior engineer


#### Extra noted 

```bash
### Name convention differs from team to team 
## One good things is to start with dev-feature 
$ git checkout -b dev-front-page

## Having good naming convention can help you build actions on Jenkins
## Target anything saying dev-* to trigger CI

## Make another job to list to deploy-*

```